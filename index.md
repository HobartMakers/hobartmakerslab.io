---
layout: default
title: Welcome
permalink: /
members: 70
---

# {{ page.title }}

<div class="pull-right" style="max-width:600px; padding-bottom:30px; margin-left:10px; margin-right:10px;">
<ul class="rslides">

{% for item in site.data.photos %}

  {% if item.featured %}
  <li>
  {% if item.url %}
  <a href="{{ site.baseurl }}/{{ item.url }}">
  {% endif %}
  <img src="{{ item.img }}" alt="{{ item.caption }}">
  {% if item.url %}
  </a>
  {% endif %}
  <p class="caption">{{ item.caption }}</p>
  </li>
  {% endif %}

{% endfor %}

</ul>
</div>

We are a group of people that love [making](https://en.wikipedia.org/wiki/Maker_culture) things. We are based in Hobart, Tasmania and we welcome people of any age, gender, skill level and from any background.

We meet up most Tuseday nights 6PM at Enterprize - 24 Davey Street Hobart.

We are run by a team of five (Leo, Michael, Paul, Alex, Dale). We have numerous regular members who join us on our Tuesday night open nights, and are active on our [Discord server](https://discord.gg/CsTrgVt7BS). **To join**, please come along to our regular open night and fill in a membership form.

## Announcements
** Hobart Makers AGM **
  
Date: 28/01/2025
  
Time: 6:00PM
  
Location: Enterprize Hobart, lounge area

Details: We excitedly announce the Hobart Makers Annual General Meeting. We will reflect on 2023, discuss our future plans and elect new committee members for the new year. We will very likely order pizzas for the occasion.