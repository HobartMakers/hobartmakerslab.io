---
layout: default
permalink: /about/contact/
---

# Contact Us

We've put the best contact methods (for us) at the top:

* We have a [discord server](https://discord.gg/CsTrgVt7BS)
* A [facebook community](https://www.facebook.com/groups/hobartmakers/)
* A [facebook page](https://www.facebook.com/HobartMakers/) (For official announcements mostly)
* You can [email](mailto:info@hobartmakers.com) us

# Find Us

We are based inside Enterprize located at: Level 5, 24 Davey Street, Hobart, Tas, 7000

Our google maps URL is [https://goo.gl/maps/HQdCryWULoR2](https://goo.gl/maps/HQdCryWULoR2), which leads to this map:

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d614.5886372928953!2d147.33083122244173!3d-42.883550096177416!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xaa6e758461eb8249%3A0x20e7cba40ff61a28!2sEnterprize!5e0!3m2!1sen!2sau!4v1538348007415" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>


