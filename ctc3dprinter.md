---
layout: equipment
title: CTC 3D Printer
permalink: /equipment/ctc3dprinter/
tag: 3dprinter
---

## Sources and Configs

 * TODO

## TODO

 * Get the 3D Printer working on Linux
 * Ability to turn off printer after print has finished
 * Add a webcam to the printer
 * Replace PC with Raspberry Pi
 * Print with 2 extruders

## SAFETY

The 3D is a dangerous piece of equipment and should only be
operated under the supervision of members who have been trained. We will run
workshops as needed to provide this training.

### Essential Rules

 * **DO NOT** put your hands or fingers in the printer while a print is in
   progress. The nozzles and bed is hot and the motors do not forgive.

## History

TODO
