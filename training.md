---
layout: default
title: Workshops / Training
permalink: /training
---
Workshops and Training
======================

To use many of the tools at the space we ask that members run through an
induction workshop on the safe use of the equipment. After you have
completed the workshop you will be given access to the equipment whenever
you want.

Workshops are run according to demand. If you would like to attend a
workshop please email training@hobartmakers.com and indicate what equipment
you want to be trained on using (eg 3D Printers, Lasercutters)
