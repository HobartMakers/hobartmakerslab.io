---
layout: default
title: Hobart Makers Merchandise
permalink: /shop
---
Hobart Makers Merchandise
=========================

The Hobart Maker's space has a small shop to buy electronics so you can
start or continue your project right away instead of having to order things
online.

Check out the Merchandise area the next time you are at the space!
