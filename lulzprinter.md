---
layout: equipment
title: LulzBot Taz4 3D Printer
permalink: /equipment/lulzprinter/
tag: 3dprinter
---

Access to the LulzBot Taz4 printer is via the OctoPrint web interface at
http://192.168.2.10/. If you are trained to use the printer you will be
given a set of login credentials to use.

# Features and Available Filaments

The TAZ4 is a reliable open source 3D extruder printer with a single print
nozzle. It uses 3mm filament and has a print volume roughly the size of a
basketball.

We have a variety of filaments available. Most of them are PLA in different
colours, but also

* Glow in the dark
* Bamboo fill - can be sanded and stained
* Bronze fill - can be polished to a dull shine

There is a webcam attached to the printer so you can view the print progress
from elsewhere in the space, and it will also make a timelapse video of your
print!

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://tube.scriptforge.org/videos/embed/b16a700b-19fe-49f9-bc79-072ee9e883b7" frameborder="0" allowfullscreen></iframe>

## SAFETY

The 3D printer is a dangerous piece of equipment with powerful stepper
motors and heating elements. To make use of the printer you will need to be
inducted in its use or you can ask another member to help you start a print
job.

## Usage Costs

When you load a model using OctoPrint you will get an estimated cost. This
is for ABS/PLA filament. If you are using a specialty filament then you will
need to load a different slicing profile which will have the cost for the
specialty filament.

In general, the cost of ABS/PLA is $0.15 per cm^3 (ABS is 1.04g/cm^3). This
covers our cost of filament, power and spare parts for the printer (eg new
heating elements and heat pads).

### Tips and Tricks

 * **DO NOT** put your hands or fingers in the printer while a print is in
   progress. The nozzles and bed is hot and the motors do not forgive.
 * Specialty filaments should be removed from the printer after the print
   has finished and the nozzle flushed with normal PLA filament. Specialty
   filaments have a habit of not remelting after prolonged heating and blocking
   the nozzle!
