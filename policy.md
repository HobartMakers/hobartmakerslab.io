---
layout: default
title: Hobart Makers Policies and Site Rules
permalink: /policy
---
Hobart Makers Policies and Site Rules
=====================================

Code Of Conduct
---------------

Hobart Makers is an inclusive organisation that aims to foster collaboration
and community to all members. People attending the space are expected to be
friendly and on good behaviour.

Any anti-social or threatening behaviour will not be tolerated and you will
be asked to leave immediately.

Please be aware that Hobart Makers brings in people from many different
backgrounds, interests and social habits. Please be mindful that your
behaviour or language could be a trigger for other members using the space
and act to cause no harm and attempt to be inclusive in your activities.

Safety First
------------

In order to use the equipment at the space you must undertake the
appropriate induction workshop on the safe use of the equipment. Some
equipment can be dangerous and you must ensured that if you are cleared to
use the equipment that you do so in a safe manner.

If you see some equipment in a state of disrepair or being handled in an
unsafe manner please alert the committee so it can be remedied.

Equipment / Parts Donations
---------------------------

Essentially, we do not encourage you bringing in your spare parts to leave
at the space for others to use. Although you may think that old monitor you
have in your collection might be useful here, the fact is that unless there
is an immediate project that someone needs it for, it is essentially junk
that is taking up space. In some cases it may cost us money to dispose of
items as they are considered e-waste.

Please DO NOT leave items at the space that are not for the immediate use of
an ongoing project.

Keeping The Space Clean
-----------------------

Users of the space are expected to keep the space clean. This includes:

- keeping any workspaces you use clean after you use them
- vacuum the floor of the workspace you use especially if you have been
  using power tools
- reporting to the committee or supervisor of any supplies that are low so
  they can be restocked
- empty the rubbish bins if you see that they are full
- put your project away in one of the filing cabinets assigned to you for
  project storage
- taking home old projects that have either been discontinued or finished
